const crypto = require('crypto');
const request = require('request');
const Promise = require('bluebird');


const _private = {};


/**
 * Generate a signature to sign API requests that require authorisation.
 * @access private
 * @param {string} path API endpoint URL suffix.
 * @param {string} queryString A querystring of parameters for the request.
 * @param {number} nonce Number of milliseconds since the Unix epoch.
 * @return {string} A string to be used as the authorisation signature.
 */
function getSignature(path, queryString, nonce) {
	const strForSign = `${path}/${nonce}/${queryString}`;
	const signatureStr = Buffer.from(strForSign).toString('base64');
	const signatureResult = crypto.createHmac('sha256', _private[this].apiSecret)
		.update(signatureStr)
		.digest('hex');
	return signatureResult;
}


/**
 * Send the request to the KuCoin API, sign if authorisation is required.
 * @access private
 * @param {string} method HTTP request method, either 'GET' or 'POST'.
 * @param {string} endpoint API endpoint URL suffix.
 * @param {boolean} [signed=false] Whether this endpoint requires authentiation.
 * @param {Object} querry Any parameters for the request.
 * @return {Promise} An object containing the API response.
 */
function rawRequest(method, endpoint, signed = false, querry) {
	const nonce = new Date().getTime();
	const queryString = querry
		? Object.entries(querry).filter(([ , value ]) => { return value !== undefined; }).map(([ key, value ]) => {
			return `${key}=${value}`;
		}).sort().join('&')
		: '';
	const options = {
		uri: `https://api.kucoin.com/v1${endpoint}?${queryString}`,
		method,
		headers: {
			'Content-Type': 'application/json',
		},
		json: true,
	};
	if (signed) {
		Object.assign(options.headers, {
			'KC-API-KEY': _private[this]._apiKey,
			'KC-API-NONCE': nonce,
			'KC-API-SIGNATURE': getSignature.call(this, `/v1${endpoint}`, queryString, nonce),
		});
	}
	return new Promise((resolve, reject) => {
		request(options, (error, response, body) => {
			if (error || !body || !body.success) {
				return reject(error || body);
			}
			return resolve(body.data);
		});
	});
}

/**
 * Do a standard public request.
 * @access private
 * @param {string} method HTTP request method, either 'GET' or 'POST'.
 * @param {string} endpoint API endpoint URL suffix.
 * @param {Object} params Any parameters for the request.
 * @return {Promise} An object containing the API response.
 */
function doRequest(method, endpoint, params) {
	return rawRequest.call(this, method, endpoint, false, params);
}

/**
 * Do a signed private request.
 * @access private
 * @param {string} method HTTP request method, either 'GET' or 'POST'.
 * @param {string} endpoint API endpoint URL suffix.
 * @param {Object} params Any parameters for the request.
 * @return {Promise} An object containing the API response.
 */
function doSignedRequest(method, endpoint, params) {
	return rawRequest.call(this, method, endpoint, true, params);
}


/**
 * A Node.js client for the KuCoin API.
 * @class
 * @version 0.0.2
 * @param {string} apiKey Your KuCoin API Key.
 * @param {string} apiSecret Your KuCoin API Secret.
 * @example
 * let kc = new Kucoin();
 */
class Kucoin {
	/**
	 * You'll need to provide your KuCoin API key and secret.
	 * @param {string} apiKey Your KuCoin API Key.
	 * @param {string} apiSecret Your KuCoin API Secret.
	 */
	constructor(apiKey, apiSecret) {
		_private[this] = {
			apiKey,
			apiSecret,
		};
	}

	/**
	 * Retrieve exchange rates for coins.
	 * @access public
	 * @param {...String} coins List of coins acronyms.
	 * @return {Promise} An object containing the API response.
	 */
	getExchangeRates(...coins) {
		return doRequest.call(this, 'GET', '/open/currencies', { coins: coins.join(',') });
	}

	/**
	 * Change the language for your account.
	 * @access public
	 * @param {String} currency Currency acronyms (USD/EUR...).
	 * @return {Promise} An object containing the API response.
	 */
	changeCurrency(currency) {
		return doSignedRequest.call(this, 'POST', '/user/change-currency', { currency });
	}

	/**
	 * Retrieve a list of supported languages.
	 * @access public
	 * @return {Promise} An object containing the API response.
	 */
	getLanguages() {
		return doRequest.call(this, 'GET', '/open/lang-list');
	}

	/**
	 * Change the language for your account.
	 * @access public
	 * @param {String} lang The specific language locale to change to from the list provided by getLanguages.
	 * @return {Promise} An object containing the API response.
	 */
	changeLanguage(lang) {
		return doSignedRequest.call(this, 'POST', '/user/change-lang', { lang });
	}

	/**
	 * Get account information for the authenticated user.
	 * @access public
	 * @return {Promise} An object containing the API response.
	 */
	getUserInfo() {
		return doSignedRequest.call(this, 'GET', '/user/info');
	}

	/**
	 * Get the number of invitees from the authenticated user's referral code.
	 * @access public
	 * @return {Promise} An object containing the API response.
	 */
	getInviteCount() {
		return doSignedRequest.call(this, 'GET', '/referrer/descendant/count');
	}

	/**
	 * Get promotion reward info.
	 * @access public
	 * @param {String} coin The coin's symbol to retrieve reward info for.
	 * @return {Promise} An object containing the API response.
	 */
	getPromotionRewardInfo(coin) {
		return doSignedRequest.call(this, 'GET', '/account/promotion/info', { coin });
	}

	/**
	 * Get promotion reward summary.
	 * @access public
	 * @param {String} coin The coin's symbol to retrieve reward summary for.
	 * @return {Promise} An object containing the API response.
	 */
	getPromotionRewardSummary(coin) {
		return doSignedRequest.call(this, 'GET', '/account/promotion/sum', { coin });
	}

	/**
	 * Retrieve the deposit address for a particular coin.
	 * @access public
	 * @param {String} coin The coin's symbol to retrieve an address for.
	 * @return {Promise} An object containing the API response.
	 */
	getDepositAddress(coin) {
		return doSignedRequest.call(this, 'GET', `/account/${coin}/wallet/address`);
	}

	/**
	 * Create a withdrawal request for the specified coin.
	 * @access public
	 * @param {String} coin The coin's symbol to create withdrawal for.
	 * @param {Number} amount Amount to withdraw.
	 * @param {String} address Deposit address.
	 * @return {Promise} An object containing the API response.
	 */
	createWithdrawal(coin, amount, address) {
		return doSignedRequest.call(this, 'POST', `/account/${coin}/withdraw/apply`, { coin, amount, address });
	}

	/**
	 * Cancel a withdrawal request for the specified coin.
	 * @access public
	 * @param {String} coin The coin's symbol to cancel withdrawal for.
	 * @param {String} txOid Identifier of the withdrawal transaction.
	 * @return {Promise} An object containing the API response.
	 */
	cancelWithdrawal(coin, txOid) {
		return doSignedRequest.call(this, 'POST', `/account/${coin}/withdraw/cancel`, { coin, txOid });
	}

	/**
	 * Retrieve deposit and withdrawal record history.
	 * @access public
	 * @param {String} coin The coin's symbol.
	 * @param {String} type DEPOSIT or WITHDRAW.
	 * @param {String} status FINISHED, CANCEL or PENDING.
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {Number} [filters.limit] Limit number of results.
	 * @param {Number} [filters.page=1] Paging.
	 * @return {Promise} An object containing the API response.
	 */
	getDepositAndWithdrawalRecords(coin, type, status, filters) {
		return doSignedRequest.call(this, 'GET', `/account/${coin}/wallet/records`, Object.assign({ coin, type, status }, filters));
	}

	/**
	 * Retrieve balance for a particular coin.
	 * @access public
	 * @param {String} coin The coin's symbol.
	 * @return {Promise} An object containing the API response.
	 */
	getBalance(coin) {
		return doSignedRequest.call(this, 'GET', `/account/${coin}/balance`);
	}

	/**
	 * Retrieve balances by pages.
	 * @access public
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {Number} [filters.limit=12] Limit number of results.
	 * @param {Number} [filters.page=1] Paging.
	 * @return {Promise} An object containing the API response.
	 */
	getBalances(filters = {}) {
		return doSignedRequest.call(this, 'GET', '/account/balance', filters);
	}

	/**
	 * Create an order for the specified trading pair.
	 * @access public
	 * @param {String} symbol The pair's symbol. (e.g.: ETH-BTC, LTC-KCS, ... Primary-Secondary).
	 * @param {String} type BUY or SELL referring to the primary.
	 * @param {Number} price Volume of Secondary coin for each Primary coin.
	 * @param {Number} amount Amount of Primary coin to sell or buy
	 * @return {Promise} An object containing the API response.
	 */
	createOrder(symbol, type, price, amount) {
		return doSignedRequest.call(this, 'POST', '/order', {
			symbol, type, price, amount,
		});
	}

	/**
	 * View a list of active orders for the specified trading pair. Using object format.
	 * @access public
	 * @param {String} symbol The pair's symbol.
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {String} [filters.type] BUY or SELL referring to the primary. If undefined both
	 * @return {Promise} An object containing the API response.
	 */
	getActiveOrders(symbol, filters) {
		return doSignedRequest.call(this, 'GET', '/order/active-map', Object.assign({ symbol }, filters));
	}

	/**
	 * Cancel an order for the specified trading pair.
	 * @access public
	 * @param {String} orderOid Order transaction ID.
	 * @param {String} symbol The pair's symbol.
	 * @param {String} type BUY or SELL referring to the primary
	 * @return {Promise} An object containing the API response.
	 */
	cancelOrder(orderOid, symbol, type) {
		return doSignedRequest.call(this, 'POST', '/cancel-order', { orderOid, symbol, type });
	}

	/**
	 * Cancel all orders for the specified trading pair.
	 * @access public
	 * @param {String} symbol The pair's symbol.
	 * @return {Promise} An object containing the API response.
	 */
	cancelOrders(symbol) {
		return doSignedRequest.call(this, 'POST', '/order/cancel-all', { symbol });
	}

	/**
	 * Retrieve a list of completed orders for the specified trading pair.
	 * @access public
	 * @param {String} symbol The pair's symbol.
	 * @param {String} type BUY or SELL referring to the primary
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {boolean} [filters.active] Is order still active ?.
	 * @param {Number} [filters.limit] Limit number of results.
	 * @param {Number} [filters.page] Paging.
	 * @param {Number} [filters.since] Timestamp (milliseconds).
	 * @param {Number} [filters.before] Timestamp (milliseconds).
	 * @return {Promise} An object containing the API response.
	 */
	getOrders(symbol, type, filters = {}) {
		return doSignedRequest.call(this, 'GET', '/orders', Object.assign({ symbol, type }, filters));
	}

	/**
	 * Retrive unique order
	 * @param {String} orderOid Order transaction ID.
	 * @param {String} symbol The pair's symbol.
	 * @param {String} type BUY or SELL referring to the primary
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {Number} [filters.limit] Limit number of results.
	 * @param {Number} [filters.page] Paging.
	 * @return {Promise} An object containing the API response.
	 */
	getOrder(orderOid, symbol, type, filters = {}) {
		return doSignedRequest.call(this, 'GET', '/order/detail', Object.assign({ orderOid, symbol, type }, filters));
	}

	/**
	 * Retrieve current price ticker data for the specified trading pair.
	 * @access public
	 * @param {String} symbol The pair's symbol.
	 * @return {Promise} An object containing the API response.
	 */
	getTicker(symbol) {
		return doRequest.call(this, 'GET', '/open/tick', { symbol });
	}

	/**
	 * Retrieve current price ticker data for all trading pair.
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {Number} [filters.market] Market.
	 * @return {Promise} An object containing the API response.
	 */
	getTickers(filters = {}) {
		return doRequest.call(this, 'GET', filters.market ? '/market/open/symbols' : '/open/tick', filters);
	}

	/**
	 * Retrieve a list of orders for the specified trading pair.
	 * @access public
	 * @param {String} symbol The pair's symbol.
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {String} [filters.type] BUY or SELL referring to the primary. If undefined both
	 * @param {Number} [filters.limit] Limit.
	 * @param {Number} [filters.group] Group (precision ?).
	 * @return {Promise} An object containing the API response.
	 */
	getOrderBooks(symbol, filters = {}) {
		filters.direction = filters.type;
		return doRequest.call(this, 'GET', `/open/orders${filters.type ? `-${filters.type.toLowerCase()}` : ''}`, Object.assign({ symbol }, filters));
	}

	/**
	 * Retrieve a list of recently completed orders for the specified trading pair.
	 * @access public
	 * @param {String} symbol The pair's symbol.
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {Number} [filters.limit] Limit.
	 * @param {Number} [filters.since] Timestamp (milliseconds).
	 * @return {Promise} An object containing the API response.
	 */
	getRecentlyDealtOrders(symbol, filters = {}) {
		return doRequest.call(this, 'GET', '/open/deal-orders', Object.assign({ symbol }, filters));
	}

	/**
	 * Retrieve a list of trending trading pairs.
	 * @access public
	 * @param {Object} [filters={}] Order details filtering.
	 * @param {Number} [filters.market] Market.
	 * @return {Promise} An object containing the API response.
	 */
	getTrending(filters = {}) {
		return doRequest.call(this, 'GET', '/market/open/coins-trending', filters);
	}

	/**
	 * Retrieve a list of available coins.
	 * @access public
	 * @param {String} coin The coin's symbol.
	 * @return {Promise} An object containing the API response.
	 */
	getCoin(coin) {
		return doRequest.call(this, 'GET', '/market/open/coin-info', { coin });
	}

	/**
	 * Retrieve a list of available coins.
	 * @access public
	 * @return {Promise} An object containing the API response.
	 */
	getCoins() {
		return doRequest.call(this, 'GET', '/market/open/coins-list');
	}
}

module.exports = Kucoin;
